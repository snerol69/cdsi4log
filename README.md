# cdsi4log

Réalisation d'un script permettant la simulation de l'exploitation de la vulnérabilité LOG4SHELL ref CVE-2021-44228.
Les différents services sont émulés au sein d'une même machine.
Merci d'utiliser une distribution debian ou dérivée. 
